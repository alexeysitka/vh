<?php
namespace AppBundle\Repository;

use AppBundle\Entity\PerfomedDiagnosis;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class PerfomedDiagnosisRepository extends EntityRepository
{
    /**
     * Возвращает список пациентов имеющих хотя бы одно выписанное лекарство
     * и имя начинающееся с указанного
     *
     * @param string $name_like Начало имени пациента
     * @param mixed $limit Количество возвращаемых результатов
     * @param mixed $offset Смещение в списке результатов
     * @param int $hydration_mode Режим гидрации результата
     * @return array Список пациентов
     */
    public function getPatientsWithMedicineAndNameBeginWith($name_like, $limit, $offset, $hydration_mode = Query::HYDRATE_ARRAY)
    {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata(PerfomedDiagnosis::class, 'p');

        $sql =
            'SELECT DISTINCT s.* ' .
            'FROM tb_source s ' .
            'INNER JOIN (' .
                'SELECT DISTINCT MEDREC_ID ' .
                'FROM tb_rel ' .
            ') r ON r.MEDREC_ID = s.MEDREC_ID ' .
            'WHERE PATIENT_NAME LIKE ? ' .
            'LIMIT ?, ?'
        ;

        $query = $this->_em->createNativeQuery($sql, $rsm);
        $query->setParameter(1, $name_like . '%')
            ->setParameter(2, intval($offset))
            ->setParameter(3, intval($limit))
        ;

        return $query->getResult($hydration_mode);
    }
}