<?php
namespace AppBundle\DataSource;

use AppBundle\Repository\PerfomedDiagnosisRepository;

class OrmPatientDataSource implements PatientDataSourceInterface
{
    protected $repository;

    public function __construct(PerfomedDiagnosisRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getPatientsWithMedicineAndNameBeginWith($name_like, $limit, $offset)
    {
        return $this->repository->getPatientsWithMedicineAndNameBeginWith($name_like, $limit, $offset);
    }
}