<?php

namespace AppBundle\DataSource;


class CachedPatientDataSource implements PatientDataSourceInterface
{
    protected $data_source;

    public function __construct(PatientDataSourceInterface $data_source)
    {
        $this->data_source = $data_source;
    }

    public function getPatientsWithMedicineAndNameBeginWith($name_like, $limit, $offset)
    {
        $hash = $this->createHash(func_get_args());

        if ($this->checkHash($hash)) {
            return $this->getCachedData($hash);
        }

        $data = $this->data_source
            ->getPatientsWithMedicineAndNameBeginWith($name_like, $limit, $offset);

        $this->saveCache($data, $hash);

        return $data;
    }

    protected function getCachedData($hash)
    {
        return unserialize(file_get_contents($this->createCachePath($hash)));
    }

    protected function checkHash($hash)
    {
        return is_file($this->createCachePath($hash));
    }

    protected function saveCache($data, $hash)
    {
        return file_put_contents($this->createCachePath($hash), serialize($data));
    }

    protected function createHash($args)
    {
        $summary = '';

        foreach ($args as $arg) {
            $summary .= $arg;
        }

        return md5($summary);
    }

    protected function createCachePath($hash)
    {
        return sys_get_temp_dir() . '/php.' . $hash . '.cache';
    }
}