<?php
namespace AppBundle\DataSource;

interface PatientDataSourceInterface
{
    public function getPatientsWithMedicineAndNameBeginWith($name_like, $limit, $offset);
}