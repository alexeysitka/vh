<?php
namespace AppBundle\DataSource;


class PdoPatientDataSource implements PatientDataSourceInterface
{
    protected $connection;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    public function getPatientsWithMedicineAndNameBeginWith($name_like, $limit, $offset)
    {
        $sql =
            'SELECT DISTINCT s.MEDREC_ID AS medrec_id, s.PATIENT_NAME AS patient_name, s.ICD as icd ' .
            'FROM tb_source s ' .
            'INNER JOIN (' .
                'SELECT DISTINCT MEDREC_ID ' .
                'FROM tb_rel ' .
            ') r ON r.MEDREC_ID = s.MEDREC_ID ' .
            'WHERE PATIENT_NAME LIKE ? ' .
            'LIMIT ?, ?'
        ;

        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue(1, $name_like . '%');
        $stmt->bindValue(2, intval($offset), \PDO::PARAM_INT);
        $stmt->bindValue(3, intval($limit), \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}