<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * PerfomedDiagnosis
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PerfomedDiagnosisRepository")
 * @ORM\Table(
 *      name="tb_source"
 * )
 */
class PerfomedDiagnosis
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="MEDREC_ID", type="string", length=10)
     */
    private $medrec_id;

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="ICD", type="string", length=10)
     */
    private $icd;

    /**
     * @var string
     *
     * @ORM\Column(name="PATIENT_NAME", type="string", length=100, nullable=true)
     */
    private $patient_name;
}