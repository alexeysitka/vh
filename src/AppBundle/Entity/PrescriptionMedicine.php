<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * PrescriptionMedicine
 *
 * @ORM\Entity
 * @ORM\Table(
 *      name="tb_rel"
 * )
 */
class PrescriptionMedicine
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="ID", type="integer", options={"unsigned":true})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="MEDREC_ID", type="string", length=10)
     */
    private $medrec_id;

    /**
     * @var string
     *
     * @ORM\Column(name="NDC", type="string", length=20)
     */
    private $ndc;
}