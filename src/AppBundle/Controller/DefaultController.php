<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PerfomedDiagnosis;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }

    /**
     * @Route(
     *      "/orm/{name_like}/{page}/{limit}",
     *      requirements={
     *          "name_like" = "[\d\w\s]+",
     *          "page" = "\d+",
     *          "limit" = "\d+"
     *      },
     *      defaults={
     *          "page" = 1,
     *          "limit" = 50
     *      },
     *      name="orm.implementation")
     * @Template("default/patient_list.html.twig")
     */
    public function ormAction($name_like, $page, $limit)
    {
        $db_page = $page > 0
            ? $page - 1
            : 0;

        return [
            'patients' => $this->container
                ->get('app.patient_datasource.orm')
                ->getPatientsWithMedicineAndNameBeginWith(
                    $name_like,
                    $limit,
                    $db_page * $limit
                ),
        ];
    }

    /**
     * @Route(
     *      "/pdo/{name_like}/{page}/{limit}",
     *      requirements={
     *          "name_like" = "[\d\w\s]+",
     *          "page" = "\d+",
     *          "limit" = "\d+"
     *      },
     *      defaults={
     *          "page" = 1,
     *          "limit" = 50
     *      },
     *      name="pdo.implementation")
     * @Template("default/patient_list.html.twig")
     */
    public function pdoAction($name_like, $page, $limit)
    {
        $db_page = $page > 0
            ? $page - 1
            : 0;

        return [
            'patients' => $this->container
                ->get('app.patient_datasource.pdo')
                ->getPatientsWithMedicineAndNameBeginWith(
                    $name_like,
                    $limit,
                    $db_page * $limit
                ),
        ];
    }

    /**
     * @Route(
     *      "/pdo_cached/{name_like}/{page}/{limit}",
     *      requirements={
     *          "name_like" = "[\d\w\s]+",
     *          "page" = "\d+",
     *          "limit" = "\d+"
     *      },
     *      defaults={
     *          "page" = 1,
     *          "limit" = 50
     *      },
     *      name="pdo_cached.implementation")
     * @Template("default/patient_list.html.twig")
     */
    public function pdoCachedAction($name_like, $page, $limit)
    {
        $db_page = $page > 0
            ? $page - 1
            : 0;

        return [
            'patients' => $this->container
                ->get('app.patient_datasource.cached_pdo')
                ->getPatientsWithMedicineAndNameBeginWith(
                    $name_like,
                    $limit,
                    $db_page * $limit
                ),
        ];
    }
}
