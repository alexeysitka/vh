<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160925115056 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tb_source CHANGE MEDREC_ID MEDREC_ID VARCHAR(10) NOT NULL, CHANGE ICD ICD VARCHAR(10) NOT NULL, ADD PRIMARY KEY (MEDREC_ID, ICD)');
        $this->addSql('ALTER TABLE tb_rel ADD ID INT UNSIGNED AUTO_INCREMENT NOT NULL FIRST, CHANGE MEDREC_ID MEDREC_ID VARCHAR(10) NOT NULL, CHANGE NDC NDC VARCHAR(20) NOT NULL, ADD PRIMARY KEY (ID)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tb_rel MODIFY ID INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE tb_rel DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE tb_rel DROP ID, CHANGE MEDREC_ID MEDREC_ID VARCHAR(10) DEFAULT NULL COLLATE utf8_general_ci, CHANGE NDC NDC VARCHAR(20) DEFAULT NULL COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE tb_source DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE tb_source CHANGE MEDREC_ID MEDREC_ID VARCHAR(10) DEFAULT NULL COLLATE utf8_general_ci, CHANGE ICD ICD VARCHAR(10) DEFAULT NULL COLLATE utf8_general_ci');
    }
}
